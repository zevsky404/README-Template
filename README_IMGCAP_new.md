<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<!-- BADGES 
If I get them to work, here are some cool badges that can be added to the readme
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url] -->



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Automatic Image Captioning</h3>

  <p align="center">
    This is the repository for the Automatic Image Captioning [ImgCap] Project at the Bauhaus University Weimar during the winter semester 2022/2023, in which we explore different approaches to the image captioning and image classification problem using various Encoder-Decoder models. (Note that this README is under construction and subject to change).
    <br />
    <a href="https://git.webis.de/code-teaching/projects/project-image-captioning-ws22"><strong>Explore the Docs »</strong></a>
    <br />
    <br />
    <!-- TODO: create API for using the model-->
    <a href="">View Demo</a>
    ·
    <a href="https://git.webis.de/code-teaching/projects/project-image-captioning-ws22/-/issues">Report Bug</a>
    ·
    <a href="https://git.webis.de/code-teaching/projects/project-image-captioning-ws22/-/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#usage">Usage</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#quicklinks">Quicklinks & Further Information</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

In the course of this project we want to explore different techniques, models and architectures for the image captioning, image classification and image retrieval problem. 

Currently, our main focus is working on a "argumentative search", meaning we wish to develop a model which, given a specific query in form of a question or a controversial statement, will provide pictures supporting the pro-side and the con-side of said query. It is supposed to find pictures that are on topic with the query, and should be able to distinguish whether an image is supporting said statement/question, or depics arguments against it. 

More information about this can be found <a href="https://touche.webis.de/clef23/touche23-web/image-retrieval-for-arguments.html">here</a>.

To explore these tasks, we have deployed different kinds of models already, and will work on our own:

- [x] a general ViT (**Vi**sion **T**ransformer) model, using a GTP2 tokenizer for taking an image and creating a caption for it
- [x] a CLIP model for, given an image and a certain amount of keywords, matching said keywords to the image with a certain probability
- [x] a DETR (**De**tection **Tr**ansformer) for detecting the objects within a given image
- [ ] a model which will take queries and find images supporting pro arguments and con arguments about said query

More information about models and vocabulary can be found in the <a href="https://git.webis.de/code-teaching/projects/project-image-captioning-ws22/-/wikis/home">wiki</a>.

The repository will document our entire journey of exploring these tasks, including papers about relevant topics and models, knowledge, as well as our development process. 

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com)-->



<p align="right">(<a href="#readme-top">back to top</a>)</p>



### Built With

* [![Huggingface][huggingface-shield]][huggingface-url]
* [![PyTorch][pytorch-shield]][pytorch-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

To run any code found in the <a href="https://git.webis.de/code-teaching/projects/project-image-captioning-ws22/-/tree/main/code">code</a> folder, you need a working Python installation for your operating system. It is also recommended to use `pip` and a virtual enviornment of your choice for installing the necessary dependencies. 

`Pip` can be installed <a href="https://pypi.org/project/pip/">here</a>.

Information about setting up a virtual enviornment using `venv` can be found <a href="https://docs.python.org/3/library/venv.html">here</a>. 

### Prerequisites

The `code` folder contains a `requirements.txt` file. It can be used to install all necessary requirements and libraries for the code to run in your virtual enviornment (find help for setting it up and activating it for your operating system in the documentation mentioned above):
  ```sh
  pip install -r requirements.txt
  ```

### Usage

After installing the necessary requirements, you can run the code in your command line with
```sh
python3 main.py
```

in the directory in which you have the files saved.

Note that running the code for the first time will take a while, as the models need to be downloaded first.

The models will all output their results for an image from the Microsoft COCO dataset of two cats. For now, to change said image requires changing the code. This will be subject to change. The models that we have deployed in this code up until now are listed above.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- ROADMAP -->
## Roadmap

**TODO**

See the [open issues](https://git.webis.de/code-teaching/projects/project-image-captioning-ws22/-/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTRIBUTING 
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p> -->



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

- elisabeth.alice.rahn@uni-weimar.de
- christiane.guetter@uni-weimar.de
- nazifa.kazimi@uni-weimar.de
- maaz.hussain@uni-weimar.de
- fatihah.ulya.hakiem@uni-weimar.de


<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- QUICKLINKS -->
## Quicklinks
 - [how to access GPUs](https://discord.com/channels/@me/1034784531246489641/1037326131046662206)
 - [Huggingface Vision Encoder/Decoder](https://huggingface.co/docs/transformers/model_doc/vision-encoder-decoder)
 - image retrieval for arguments: [[demo](https://images.args.me/)] [[touche 2022](https://touche.webis.de/clef22/touche22-web/image-retrieval-for-arguments.html)] [[touche 2023](https://touche.webis.de/clef23/touche23-web/image-retrieval-for-arguments.html)]



<!-- ACKNOWLEDGMENTS 
## Acknowledgments

* []()
* []()
* []()

<p align="right">(<a href="#readme-top">back to top</a>)</p> -->




<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[contributors-shield]: https://img.shields.io/github/contributors/github_username/repo_name.svg?style=for-the-badge
[contributors-url]: https://git.webis.de/code-teaching/projects/project-image-captioning-ws22/-/graphs/main

[forks-shield]: https://img.shields.io/github/forks/github_username/repo_name.svg?style=for-the-badge
[forks-url]: https://git.webis.de/code-teaching/projects/project-image-captioning-ws22/-/forks

[stars-shield]: https://img.shields.io/github/stars/github_username/repo_name.svg?style=for-the-badge
[stars-url]: https://github.com/github_username/repo_name/stargazers

[issues-shield]: https://img.shields.io/github/issues/github_username/repo_name.svg?style=for-the-badge
[issues-url]: https://github.com/github_username/repo_name/issues

[license-shield]: https://img.shields.io/github/license/github_username/repo_name.svg?style=for-the-badge
[license-url]: https://github.com/github_username/repo_name/blob/master/LICENSE.txt

[product-screenshot]: images/screenshot.png

[huggingface-shield]: https://img.shields.io/badge/Huggingface-%23FFC300?style=for-the-badge
[huggingface-url]: https://huggingface.co/

[pytorch-shield]: https://img.shields.io/badge/PyTorch-%23C70039?style=for-the-badge
[pytorch-url]: https://pytorch.org/
